var REST = {

    /**
     *
     * @param httpMethod HTTP Method: GET, POST, DELETE, PUT
     * @param dataSent javascript object to be sent
     * @param urlSent url to be used
     * @param onError function called on error
     */
    sendData: function(httpMethod, dataSent, urlSent, onError, onSuccess) {
        $.ajax({
            type: httpMethod,
            contentType: "application/json",
            data: JSON.stringify(dataSent),
            url: urlSent,
            error: onError,
            success: onSuccess
        });
    },

    /**
     *
     * @param urlSent url to be used
     * @param onError function called on error
     * @param onSuccess function called on success
     */
    getData: function(urlSent, onError, onSuccess) {
        $.ajax({
            type: "GET",
            url: urlSent,
            error: onError,
            success: onSuccess
        });
    },

    /**
     *
     * @param httpMethod HTTP Method: GET, POST, DELETE, PUT
     * @param dataSent javascript object to be sent
     * @param urlSent url to be used
     * @param onError function called on error
     * @param onSuccess function called on success
     */
    fullForm: function(httpMethod, dataSent, urlSent, onError, onSuccess) {
        $.ajax({
            type: httpMethod,
            contentType: "application/json",
            data: JSON.stringify(dataSent),
            url: urlSent,
            success: onSuccess,
            error: getDefaultOnErrorFunctionIfNotPresent(onError)
        });
    },

    getModal: function(modalId, urlSent, onError) {
        $('#'+modalId).modal();

        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: urlSent,
            success: function (data) {
                $('#' + modalId + ' .modal-body').html(data);
            },
            error: getDefaultOnErrorFunctionIfNotPresent(onError)
        });
    },
    replaceWithRequestResponse(replacedId, urlSent, onError) {
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: urlSent,
            success: function (data) {
                $('#'+replacedId).html(data);
            },
            error: getDefaultOnErrorFunctionIfNotPresent(onError)
        });
    }
};

function getDefaultOnErrorFunctionIfNotPresent(onError) {
    if(onError == null) {
        onError = function(jqXHR, textStatus, errorThrown){
            alert("error occured! Status: " + textStatus);
            console.log(jqXHR);
        }
    }

    return onError;
}