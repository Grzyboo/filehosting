function submitSearch() {
    var searchPattern = $("#search-input").val();
    REST.getData('/search/by/name/' + searchPattern, null, function(data) {
        $('#search-results').html(data);
    })
}