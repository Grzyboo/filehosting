function uploadFile(file) {
    var formData = new FormData();
    formData.append("file", file);
    formData.append("repositoryId", '' + selectedRepositoryId);

    $.ajax({
        url: '/files/upload',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            $('#uploaded_files').show();
            $('#uploaded_files_body').append(data);
        }
    });
}

$(document).ready(function() {
    $('#file_upload_form').submit(function(event) {
        event.preventDefault();
        var filesInput = $('#file_upload_input');
        var files = filesInput.prop('files');
        uploadFile(files[0]);
        filesInput.val('');
    });
});
