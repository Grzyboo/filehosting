package web.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "t_user_settings")
@Data
public class UserSettings {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    Long id;

    String theme;
    String language;

    @OneToOne
    @JoinColumn(name = "user_id")
    User user;


}
