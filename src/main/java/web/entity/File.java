package web.entity;

import lombok.Data;
import org.hibernate.annotations.Where;
import web.dto.SizedItemDto;
import web.enums.FileType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_file")
@Data
@Where(clause="deleted != true")
public class File extends SizedItemDto {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    Long id;

    Boolean deleted;
    String name;
    String extension;
    Long size;
    Date created;
    Date expires;

    @Enumerated(value=EnumType.STRING)
    FileType fileType;


    @OneToOne
    Repository repository;
    
}