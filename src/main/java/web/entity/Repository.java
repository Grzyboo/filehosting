package web.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "t_repository")
@Data
public class Repository {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    Long id;

    Boolean deleted;
    String path;
    String password;
    Boolean isProtected;
    Boolean isPrivate;

    @OneToOne
    @JoinColumn(name = "owner_id")
    User owner;

}