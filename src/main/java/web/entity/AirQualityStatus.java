package web.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_air_quality_status")
@Data
public class AirQualityStatus {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    Long id;

    String city;
    String status;
    Double aqi;
    Date time;
}
