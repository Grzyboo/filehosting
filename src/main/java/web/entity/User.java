package web.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "t_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = "userSettings")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    Long id;

    String displayName;
    String email;
    String password;

    @OneToOne(mappedBy = "user", cascade=CascadeType.ALL)
    UserSettings userSettings;

}
