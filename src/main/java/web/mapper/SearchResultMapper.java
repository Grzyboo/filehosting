package web.mapper;

import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;
import web.dto.SearchResultDto;
import web.entity.File;

@Component
public class SearchResultMapper implements BaseMapper {

    @Override
    public void configure(MapperFactory factory) {
        factory.classMap(File.class, SearchResultDto.class)
                .field("repository.owner.id", "ownerId")
                .field("repository.owner.displayName", "ownerName")
                .field("repository.path", "repositoryPath")
                .byDefault()
                .register();
    }
}
