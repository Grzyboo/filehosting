package web.mapper;

import ma.glasnost.orika.MapperFactory;

public interface BaseMapper {
    void configure(MapperFactory factory);
}
