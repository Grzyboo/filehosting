package web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import web.entity.Repository;

import java.util.List;

@org.springframework.stereotype.Repository
public interface RepositoryRepo extends JpaRepository<Repository, Long> {
    long countAllByPathAndOwnerId(String path, Long ownerId);
    long countAllByPathAndIdNotAndOwnerId(String path, Long id, Long ownerId);

    List<Repository> getAllByOwnerIdAndDeletedFalse(Long userId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE t_repository SET deleted = :deleted WHERE id = :id", nativeQuery = true)
    void setRepositoryDeleted(@Param("id") Long id, @Param("deleted") Boolean deleted);
}
