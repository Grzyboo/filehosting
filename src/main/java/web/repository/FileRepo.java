package web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import web.entity.File;

import java.util.List;

public interface FileRepo extends JpaRepository<File, Long> {

    @Query("SELECT f FROM File f WHERE f.repository.owner.id = :id")
    List<File> findByOwnerId(@Param("id") Long id);

    @Query("SELECT f FROM File f WHERE f.repository.id = :id")
    List<File> findByRepositoryId(@Param("id") Long id);

    @Query("SELECT f FROM File f WHERE f.repository.isPrivate <> TRUE AND f.name LIKE :nameLike% ORDER BY created desc")
    List<File> searchByNameLikeOrderByCreatedDesc(@Param("nameLike") String nameLike);

    @Modifying
    @Query("UPDATE File f SET f.deleted = :deleted WHERE f.id = :id")
    void setDeleted(@Param("id") Long id, @Param("deleted") boolean deleted);

    @Modifying
    @Query("UPDATE File f SET f.deleted = true WHERE f.repository.id = :repoId")
    void deleteAllFilesWhereRepositoryId(@Param("repoId") Long id);
}
