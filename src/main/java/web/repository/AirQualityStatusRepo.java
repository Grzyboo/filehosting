package web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import web.entity.AirQualityStatus;

public interface AirQualityStatusRepo extends JpaRepository<AirQualityStatus, Long> {

    @Query(value = "SELECT * FROM t_air_quality_status ORDER BY time DESC LIMIT 1", nativeQuery = true)
    AirQualityStatus findLast();
}
