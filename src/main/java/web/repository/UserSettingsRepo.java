package web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import web.entity.UserSettings;

public interface UserSettingsRepo extends JpaRepository<UserSettings, Long> {
    UserSettings getOneByUserId(long userId);

    @Modifying
    @Query("update UserSettings u set u.language = :lang, u.theme = :theme where u.user.id = :userId")
    void updateUserSettings(@Param("userId") long userId, @Param("lang") String lang, @Param("theme") String theme);
}
