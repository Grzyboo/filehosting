package web.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import web.entity.Repository;
import web.entity.User;
import web.repository.RepositoryRepo;
import web.repository.UserRepo;
import web.security.SecurityUser;

@Component
public class LoggedInUserUtil {

    @Value("${file.hosting.security.enable}")
    private Boolean security;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RepositoryRepo repositoryRepo;

    public User get() {
        if(security) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            if (authentication == null)
                return null;

            String name = authentication.getName();
            return userRepo.findByEmail(name);
        }
        else {
            User user = userRepo.findByEmail("example@gmail.com");
            if(user == null) {
                user = User.builder()
                        .id(1L)
                        .displayName("user")
                        .password("password")
                        .email("example@gmail.com")
                        .build();
                userRepo.save(user);
            }

            return user;
        }
    }

    public long getId() {
        return get().getId();
    }

    public String getStringId() {
        return String.valueOf(get().getId());
    }

    public SecurityUser getSecurityUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth == null)
            return null;

        return (SecurityUser) auth.getPrincipal();
    }

    public boolean hasAccessToRepository(Long repoId) {
        Repository repository = repositoryRepo.getOne(repoId);

        if(!repository.getIsProtected())
            return true;

        SecurityUser securityUser = getSecurityUser();

        if(securityUser != null) {
            if(securityUser.getAccessedRepos().contains(repoId)) {
                return true;
            }
            else {
                User user = get();

                return repository.getOwner().getId().equals(user.getId());
            }
        }

        return false;
    }
}
