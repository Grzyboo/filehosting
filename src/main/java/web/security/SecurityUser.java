package web.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class SecurityUser extends User {
    private List<Long> accessedRepoIds = new LinkedList<>();

    public SecurityUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public SecurityUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public List<Long> getAccessedRepos() {
        //Long[] array = new Long[accessedRepoIds.size()];
        //return accessedRepoIds.toArray(array);
        return new LinkedList<>(accessedRepoIds);
    }

    public void addRepoAccess(Long repoId) {
        accessedRepoIds.add(repoId);
    }
}
