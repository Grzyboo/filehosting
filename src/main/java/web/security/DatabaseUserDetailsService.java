package web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import web.entity.User;
import web.repository.UserRepo;

import java.util.ArrayList;
import java.util.List;

@Service
public class DatabaseUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepo repo;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = repo.findByEmail(s);

        if(user == null)
            throw new UsernameNotFoundException("No user found with username: " + s);

        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");

        return new SecurityUser(user.getEmail(), user.getPassword(), true, true, true, true, getAuthorities(roles));
    }

    private static List<GrantedAuthority> getAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for(String role : roles)
            authorities.add(new SimpleGrantedAuthority(role));

        return authorities;
    }
}
