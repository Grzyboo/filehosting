package web.validator;

import web.dto.RegisterUserDto;
import web.validator.annotation.ValidPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<ValidPassword, Object> {

    @Override
    public void initialize(ValidPassword constraintAnnotation) {}

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        RegisterUserDto user = (RegisterUserDto) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }
}