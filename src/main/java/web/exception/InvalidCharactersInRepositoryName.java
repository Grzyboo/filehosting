package web.exception;

public class InvalidCharactersInRepositoryName extends RuntimeException {
    public InvalidCharactersInRepositoryName(String message) {
        super(message);
    }
}
