package web.exception;

public class RepositoryAlreadyExistsException extends Exception {
    public RepositoryAlreadyExistsException(String message) {
        super("Repository with path: " + message + " already exists");
    }
}
