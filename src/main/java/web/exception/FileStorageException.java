package web.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@Slf4j
public class FileStorageException extends RuntimeException {
    public FileStorageException(String message) {
        super(message);
        log.info(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
