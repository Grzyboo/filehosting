package web.exception;

public class EmailAlreadyExistsException extends Exception {
    private String email;

    public EmailAlreadyExistsException(String email) {
        super("Email '" + email + "' already exists");
        this.email = email;
    }
}
