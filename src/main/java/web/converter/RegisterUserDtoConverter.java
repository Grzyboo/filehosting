package web.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import web.dto.RegisterUserDto;
import web.entity.User;
import web.entity.UserSettings;

@Component
public class RegisterUserDtoConverter implements EntityConverter<User, RegisterUserDto> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User convertToEntity(RegisterUserDto registerUserDto) {
        return User.builder()
                    .displayName(registerUserDto.getDisplayName())
                    .email(registerUserDto.getEmail())
                    .password(passwordEncoder.encode(registerUserDto.getPassword()))
                .build();
    }
}
