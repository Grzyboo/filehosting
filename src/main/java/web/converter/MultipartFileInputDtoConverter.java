package web.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import web.dto.MultipartFileInputDto;
import web.entity.File;
import web.enums.FileType;
import web.repository.RepositoryRepo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class MultipartFileInputDtoConverter implements EntityConverter<File, MultipartFileInputDto> {

    @Autowired
    RepositoryRepo repositoryRepo;

    @Override
    public File convertToEntity(MultipartFileInputDto dto) {
        MultipartFile mFile = dto.getFile();
        String fileName = mFile.getOriginalFilename() == null ? "" : mFile.getOriginalFilename();
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);

        File file = new File();
        file.setDeleted(false);
        file.setName(fileName);
        file.setExtension(extension);
        file.setSize(mFile.getSize());
        file.setCreated(new Date());
        file.setExpires(null); // TODO
        file.setFileType(getFileType(extension));
        file.setRepository(repositoryRepo.getOne(dto.getRepositoryId()));

        return file;
    }

    private FileType getFileType(String extension) {
        Map<String, FileType> map = getMapping();

        FileType type = map.get(extension);
        if(type == null)
            type = FileType.OTHER;

        return type;
    }

    private Map<String, FileType> getMapping() {
        Map<String, FileType> map = new HashMap<>();
        map.put("jpg", FileType.IMAGE);
        map.put("png", FileType.IMAGE);
        map.put("gif", FileType.IMAGE);

        map.put("mp3", FileType.AUDIO);
        map.put("wav", FileType.AUDIO);

        map.put("txt", FileType.TEXT);

        map.put("mp4", FileType.VIDEO);
        map.put("avi", FileType.VIDEO);
        map.put("wmv", FileType.VIDEO);

        map.put("rar", FileType.ARCHIVE);
        map.put("zip", FileType.ARCHIVE);
        map.put("7z", FileType.ARCHIVE);

        return map;
    }
}
