package web.converter;

public interface EntityConverter<ENTITY, DTO> {
    ENTITY convertToEntity(DTO dto);
}

