package web.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import web.dto.RepositoryUpdateDto;
import web.entity.Repository;
import web.repository.RepositoryRepo;

@Component
public class RepositoryUpdateDtoConverter implements EntityConverter<Repository, RepositoryUpdateDto> {

    @Autowired
    RepositoryRepo repo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Repository convertToEntity(RepositoryUpdateDto dto) {
        Repository repository = repo.getOne(dto.getId());

        repository.setPath(dto.getPath());
        repository.setIsPrivate(dto.getIsPrivate());
        repository.setIsProtected(dto.getIsProtected());
        repository.setPassword(passwordEncoder.encode(dto.getPassword()));

        return repository;
    }
}
