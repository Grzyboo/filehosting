package web.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import web.dto.RepositoryCreationDto;
import web.entity.Repository;
import web.util.LoggedInUserUtil;

@Component
public class RepositoryCreationDtoConverter implements EntityConverter<Repository, RepositoryCreationDto> {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    LoggedInUserUtil loggedInUserUtil;

    @Override
    public Repository convertToEntity(RepositoryCreationDto dto) {
        Repository repository = new Repository();

        if(dto.getIsProtected() && dto.getPassword() != null) {
            repository.setPassword(passwordEncoder.encode(dto.getPassword()));
            repository.setIsProtected(true);
        }
        else repository.setIsProtected(false);

        repository.setIsPrivate(dto.getIsPrivate());
        repository.setDeleted(false);
        repository.setOwner(loggedInUserUtil.get());

        return repository;
    }
}
