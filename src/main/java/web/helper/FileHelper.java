package web.helper;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import web.entity.File;

public class FileHelper {
    private FileHelper() {}

    public static String getFullFilePath(File file) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/files/download/")
                .path(String.valueOf(file.getId()))
                .toUriString();
    }

    public static String getFileInfoPath(File file) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/files/info/")
                .path(String.valueOf(file.getId()))
                .toUriString();
    }
}
