package web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import web.dto.RepositoryCreationDto;
import web.dto.RepositoryDto;
import web.dto.RepositoryUpdateDto;
import web.exception.RepositoryAlreadyExistsException;
import web.service.RepositoryService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/repository")
@Slf4j
public class RepositoryController extends BaseController {

    @Autowired
    RepositoryService service;

    @GetMapping("/my")
    public String myRepositories(Model model) {
        model.addAttribute("repos", service.getDisplayRepositories(getLoggedInUser().getId()));
        return "fragment/my_repositories";
    }

    @ResponseBody
    @GetMapping("/get")
    public List<RepositoryDto> getAllRepositories() {
        return service.getAllRepositories(getLoggedInUser().getId());
    }


    @ResponseBody
    @PutMapping("/create")
    public void create(@RequestBody RepositoryCreationDto repositoryCreationDto) {
        try {
            service.create(repositoryCreationDto);
        } catch (RepositoryAlreadyExistsException e) {
            e.printStackTrace();
        }
    }

    @ResponseBody
    @PostMapping("/update")
    public void update(@RequestBody RepositoryUpdateDto repositoryUpdateDto) {
        try {
            service.update(repositoryUpdateDto);
        } catch (RepositoryAlreadyExistsException e) {
            e.printStackTrace();
        }
    }

    @ResponseBody
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }

    @ResponseBody
    @GetMapping("/permission/{repoId}/password/{password}")
    public void requestRepositoryAccess(@PathVariable Long repoId, @PathVariable String password, Principal p) {
        log.info(p.getName() + " requested access for repository: " + repoId);
        service.requestRepositoryAccess(repoId, password);
    }
}
