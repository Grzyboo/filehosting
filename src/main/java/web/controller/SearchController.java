package web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import web.service.SearchService;

@Controller
@RequestMapping("/search")
@Slf4j
public class SearchController extends BaseController {

    @Autowired
    SearchService searchService;

    @GetMapping("")
    public String search(Model model) {
        return getView("search", model, "title.search");
    }

    @GetMapping("/by/name/{name}")
    public String searchByName(@PathVariable String name, Model model) {
        model.addAttribute("searchResults", searchService.getResultsByFileName(name));
        return "items/search_results";
    }
}
