package web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import web.dto.UserSettingsDto;
import web.service.UserSettingsService;

@Controller
@RequestMapping("/settings")
@Slf4j
public class UserSettingsController extends BaseController {

    @Autowired
    UserSettingsService userSettingsService;

    @GetMapping("/me")
    public String mySettings(Model model) {
        String[] langs = {"Polski", "English"};
        String[] langsShort = {"pl", "en"};

        model.addAttribute("settings", userSettingsService.getUserSettings(getLoggedInUserId()));
        model.addAttribute("langs", langs);
        model.addAttribute("langsShort", langsShort);
        return getView("my_settings", model, "title.user.settings");
    }

    @PostMapping("/update")
    @ResponseBody
    public void update(@RequestBody UserSettingsDto userSettingsDto) {
        userSettingsService.update(getLoggedInUserId(), userSettingsDto);
    }
}
