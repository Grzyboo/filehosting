package web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import web.service.AirQualityService;

@Controller
@RequestMapping("/air")
@Slf4j
public class AirQualityStatusController extends BaseController {

    @Autowired
    AirQualityService service;

    @GetMapping("/get")
    public String getPage(Model model) {
        model.addAttribute("status", service.getLastStatus());
        return getView("air_quality", model, "title.air.quality");
    }

}
