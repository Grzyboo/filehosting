package web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import web.dto.RegisterUserDto;
import web.exception.EmailAlreadyExistsException;
import web.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@Slf4j
public class RegisterController extends BaseController {

    @Autowired
    private UserService service;

    @GetMapping("/login")
    public String login(Model model) {
        addBasicAttributes(model);
        return "login";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("user", new RegisterUserDto());
        addBasicAttributes(model);
        return "registerPage";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("user") @Valid RegisterUserDto accountDto, BindingResult result, Model model, HttpServletRequest request) {
        if(result.hasErrors()) {
            model.addAttribute("user", accountDto);
            addBasicAttributes(model);
            return "registerPage";
        }
        else {
            try {
                service.registerNewUser(accountDto);
            } catch (EmailAlreadyExistsException e) {
                model.addAttribute("user", accountDto);
                addBasicAttributes(model);
                return "registerPage";
            }
        }

        try {
            request.login(accountDto.getEmail(), accountDto.getPassword());
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return getView("home", model, "title.home");
    }

}
