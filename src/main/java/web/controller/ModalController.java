package web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import web.dto.RepositoryDto;
import web.service.RepositoryService;

@Controller
@RequestMapping("/modal")
@Slf4j
public class ModalController extends BaseController {
    private static final String FRAGMENT_LOCATION = "fragment/modals::";

    @Autowired
    RepositoryService repositoryService;

    @GetMapping("/repository/edit/{id}")
    public String getRepositoryEditModal(Model model, @PathVariable Long id) {
        RepositoryDto repositoryDto = repositoryService.getById(id);
        model.addAttribute("repository", repositoryDto);
        return FRAGMENT_LOCATION + "edit_repo";
    }
}
