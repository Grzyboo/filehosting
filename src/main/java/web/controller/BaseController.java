package web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import web.entity.User;
import web.util.LoggedInUserUtil;

public abstract class BaseController {

    @Autowired
    private LoggedInUserUtil loggedInUserUtil;

    User getLoggedInUser() {
        return loggedInUserUtil.get();
    }

    long getLoggedInUserId() {
        return loggedInUserUtil.get().getId();
    }

    protected void addBasicAttributes(Model model) {
        User user = getLoggedInUser();

        String themeCss = "light.css";

        if(user != null) {
            model.addAttribute("loggedInUsername", user.getDisplayName());

            String theme = user.getUserSettings().getTheme();
            if(theme.equals("dark"))
                themeCss = "dark.css";
        }
        else model.addAttribute("loggedInUsername", null);

        model.addAttribute("appTheme", "themes/" + themeCss);
    }

    protected String getView(String contentFragment, Model model, String title) {
        model.addAttribute("title", title);
        model.addAttribute("content", contentFragment);
        addBasicAttributes(model);
        return "template_main";
    }

    protected String getView(String contentFragment, Model model) {
        return getView(contentFragment, model, null);
    }
}
