package web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import web.dto.MultipartFileInputDto;
import web.dto.UploadFileResponseDto;
import web.entity.File;
import web.helper.FileHelper;
import web.service.FileService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/files")
@Slf4j
public class FileController extends BaseController {

    @Autowired
    private FileService fileService;

    @GetMapping("/my")
    public String getMyFiles(Model model) {
        return getView("my_files", model, "title.my.files");
    }

    @GetMapping("/my/repo/{repositoryId}")
    public String getMyFiles(Model model, @PathVariable Long repositoryId) {
        List<File> files = fileService.getFilesFromRepository(repositoryId);
        model.addAttribute("files", files);
        model.addAttribute("uris", files.stream().map(FileHelper::getFileInfoPath).collect(Collectors.toList()));
        return "items/my_files_table";
    }

    @GetMapping("/add")
    public String addFiles(Model model) {
        return getView("add_files", model, "title.add.files");
    }

    @GetMapping("/info/{fileId}")
    public String getFileInfo(@PathVariable Long fileId, Model model) {
        model.addAttribute("info", fileService.getFileById(fileId));
        return getView("file_info", model,"title.file.info");
    }

    @PostMapping(value = "/upload", consumes = {"multipart/form-data"})
    public String upload(@RequestPart("file") MultipartFile file, @RequestPart("repositoryId") String repositoryId, Model model) {
        MultipartFileInputDto dto = new MultipartFileInputDto(file, Long.parseLong(repositoryId));
        UploadFileResponseDto responseDto = fileService.storeFile(dto);

        model.addAttribute("response", responseDto);
        return "items/uploaded_file_row";
    }

    @PostMapping("/upload/multiple")
    @ResponseBody
    public List<UploadFileResponseDto> uploadMultiple(@RequestParam("files") MultipartFile[] files) {
        return Arrays.stream(files)
                .map(file -> fileService.storeFile(new MultipartFileInputDto(file, 1L)))
                .collect(Collectors.toList());
        // TODO
    }

    @GetMapping("/download/{fileId}")
    @ResponseBody
    public ResponseEntity<Resource> download(@PathVariable Long fileId) {
        // TODO check if user can see this file

        Resource resource = fileService.loadFileAsResource(fileId);
        String fileName = fileService.getFileName(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/octet-stream")) // TODO: change content type to use saved value
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(resource);
    }

    @DeleteMapping("/delete/{fileId}")
    @ResponseBody
    public void delete(@PathVariable("fileId") Long fileId) {
        fileService.deleteFile(fileId);
    }
}
