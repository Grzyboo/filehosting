package web.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import web.converter.RepositoryCreationDtoConverter;
import web.converter.RepositoryUpdateDtoConverter;
import web.dto.RepositoryCreationDto;
import web.dto.RepositoryDisplayDto;
import web.dto.RepositoryDto;
import web.dto.RepositoryUpdateDto;
import web.entity.Repository;
import web.exception.InvalidCharactersInRepositoryName;
import web.exception.RepositoryAlreadyExistsException;
import web.repository.FileRepo;
import web.repository.RepositoryRepo;
import web.security.SecurityUser;
import web.service.repository.display.RepositoryDisplayDtoListConverter;
import web.util.LoggedInUserUtil;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
public class RepositoryService extends BaseService {

    @Autowired
    RepositoryRepo repo;

    @Autowired
    FileRepo fileRepo;

    @Autowired
    RepositoryCreationDtoConverter repositoryCreationDtoConverter;

    @Autowired
    RepositoryUpdateDtoConverter repositoryUpdateDtoConverter;

    @Autowired
    LoggedInUserUtil loggedInUserUtil;

    @Autowired
    PasswordEncoder passwordEncoder;

    public RepositoryDto getById(Long id) {
        return convert(repo.getOne(id), RepositoryDto.class);
    }

    public List<RepositoryDisplayDto> getDisplayRepositories(Long ownerId) {
        List<Repository> repos = repo.getAllByOwnerIdAndDeletedFalse(ownerId);

        RepositoryDisplayDtoListConverter converter = new RepositoryDisplayDtoListConverter();
        repos.forEach(converter);
        return converter.getAll();
    }

    public List<RepositoryDto> getAllRepositories(Long id) {
        return convert(repo.getAllByOwnerIdAndDeletedFalse(id), RepositoryDto.class);
    }

    public void create(RepositoryCreationDto dto) throws RepositoryAlreadyExistsException {
        String parentPath = repo.getOne(dto.getParentId()).getPath();
        String name = dto.getName();

        if(name.contains("\\") || name.contains("/"))
            throw new InvalidCharactersInRepositoryName("Found invalid characters: \\ or /");

        String path = parentPath + name + "/";
        if(repositoryAlreadyExists(path)) {
            throw new RepositoryAlreadyExistsException(path);
        }

        Repository repository = repositoryCreationDtoConverter.convertToEntity(dto);
        repository.setPath(path);
        repo.save(repository);
    }

    private boolean repositoryAlreadyExists(String path) {
        return repo.countAllByPathAndOwnerId(path, loggedInUserUtil.getId()) != 0;
    }

    public void update(RepositoryUpdateDto dto) throws RepositoryAlreadyExistsException {
        if(repositoryAlreadyExists(dto)) {
            throw new RepositoryAlreadyExistsException(dto.getPath());
        }

        if(!repositoryOwnerMatches(dto)) {
            throw new AccessDeniedException("Access denied when updating repository: " + dto.getPath());
        }

        Repository repository = repositoryUpdateDtoConverter.convertToEntity(dto);
        repo.save(repository);
    }

    private boolean repositoryAlreadyExists(RepositoryUpdateDto dto) {
        return repo.countAllByPathAndIdNotAndOwnerId(dto.getPath(), dto.getId(), loggedInUserUtil.getId()) != 0;
    }

    private boolean repositoryOwnerMatches(RepositoryUpdateDto dto) {
        long userId = loggedInUserUtil.get().getId();
        long repositoryOwnerId = repo.getOne(dto.getId()).getOwner().getId();

        return userId == repositoryOwnerId;
    }

    @Transactional
    public void delete(Long id) {
        Repository repository = repo.getOne(id);
        if(repository.getPath().equals("root/"))
            return;
        
        fileRepo.deleteAllFilesWhereRepositoryId(id);
        repo.setRepositoryDeleted(id, true);
    }

    public void requestRepositoryAccess(Long repoId, String password) {
        Repository repository = repo.getOne(repoId);
        SecurityUser userDetails = loggedInUserUtil.getSecurityUser();

        if(passwordEncoder.matches(password, repository.getPassword())) {
            if(userDetails != null) {
                if (!userDetails.getAccessedRepos().contains(repoId)) {
                    userDetails.addRepoAccess(repoId);
                    log.info(userDetails.getUsername() + " gained access to repository: " + repoId);
                }
            }
        }
        else {
            log.info(userDetails.getUsername() + " failed to get access for repository: " + repoId);
        }
    }
}
