package web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.dto.SearchResultDto;
import web.entity.File;
import web.helper.FileHelper;
import web.repository.FileRepo;
import web.util.LoggedInUserUtil;

import java.util.List;

@Service
public class SearchService extends BaseService {

    @Autowired
    FileRepo fileRepo;

    @Autowired
    LoggedInUserUtil loggedInUserUtil;

    public List<SearchResultDto> getResultsByFileName(String name) {
        List<File> files = fileRepo.searchByNameLikeOrderByCreatedDesc(name);
        List<SearchResultDto> dtos = convert(files, SearchResultDto.class);

        int i = 0;
        for(SearchResultDto resultDto : dtos) {
            File f = files.get(i++);

            if(!loggedInUserUtil.hasAccessToRepository(f.getRepository().getId()))
                resultDto.addAccessNeededInformation();

            resultDto.setUri(FileHelper.getFileInfoPath(f));
        }

        return dtos;
    }
}
