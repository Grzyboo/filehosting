package web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import web.converter.MultipartFileInputDtoConverter;
import web.dto.*;
import web.entity.File;
import web.entity.Repository;
import web.entity.User;
import web.exception.FileStorageException;
import web.helper.FileHelper;
import web.properties.FileHostingProperties;
import web.repository.FileRepo;
import web.util.LoggedInUserUtil;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Service
public class FileService extends BaseService {
    private final Path storageLocation;

    @Autowired
    FileRepo fileRepo;

    @Autowired
    LoggedInUserUtil loggedInUserUtil;

    @Autowired
    FileHostingProperties fileHostingProperties;

    @Autowired
    MultipartFileInputDtoConverter multipartFileInputDtoConverter;

    @Autowired
    public FileService(FileHostingProperties fileHostingProperties) {
        storageLocation = Paths.get(fileHostingProperties.getUploadDir()).toAbsolutePath().normalize();

        try {
            Files.createDirectories(storageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Error creating uploads directory (" + fileHostingProperties.getUploadDir() + ")", ex);
        }
    }

    public FileInfoDto getFileById(Long fileId) {
        File file = fileRepo.getOne(fileId);
        SearchResultDto searchResultDto = convert(file, SearchResultDto.class);
        searchResultDto.setUri(FileHelper.getFileInfoPath(file));

        FileProtectionDto protection = null;
        Repository fileRepository = file.getRepository();
        if(fileRepository.getIsProtected()) {
            Long repoId = fileRepository.getId();
            protection = new FileProtectionDto(repoId, fileRepository.getPath(), loggedInUserUtil.hasAccessToRepository(repoId));
        }
        return new FileInfoDto(searchResultDto, FileHelper.getFullFilePath(file), protection);
    }

    public UploadFileResponseDto storeFile(MultipartFileInputDto fileInput) {
        MultipartFile file = fileInput.getFile();
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String userId = loggedInUserUtil.getStringId();

        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Filename contains invalid path sequence " + fileName);
            }

            Path targetLocation = storageLocation.resolve(userId);
            Files.createDirectories(targetLocation);

            File createdFile = multipartFileInputDtoConverter.convertToEntity(fileInput);
            createdFile = fileRepo.save(createdFile);

            Path fileLocation = targetLocation.resolve(String.valueOf(createdFile.getId()));
            Files.copy(file.getInputStream(), fileLocation, StandardCopyOption.REPLACE_EXISTING);

            return new UploadFileResponseDto(fileName, FileHelper.getFullFilePath(createdFile), FileHelper.getFileInfoPath(createdFile), file.getContentType(), file.getSize());

        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(Long fileId) {
        try {
            File file = fileRepo.getOne(fileId);
            if(file == null)
                throw new FileStorageException("File #" + fileId + " not found in the database");

            checkRepositoryPermission(file.getRepository());

            Path filePath = storageLocation.resolve(String.valueOf(file.getRepository().getOwner().getId())).resolve(String.valueOf(fileId)).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(!resource.exists())
                throw new FileStorageException("File " + filePath + " not found");

            return resource;
        } catch (MalformedURLException ex) {
            throw new FileStorageException("File #" + fileId + " not found");
        }
    }

    private void checkRepositoryPermission(Repository repository) {
        boolean access = loggedInUserUtil.hasAccessToRepository(repository.getId());

        if(!access)
            throw new AccessDeniedException("Cannot access repository " + repository);
    }

    public String getFileName(Long fileId) {
        return fileRepo.getOne(fileId).getName();
    }

    public List<File> getFilesFromRepository(Long repositoryId) {
        return fileRepo.findByRepositoryId(repositoryId);
    }

    @Transactional
    public void deleteFile(Long fileId) {
        File file = fileRepo.getOne(fileId);
        if(isOwner(file, loggedInUserUtil.get())) {
            fileRepo.setDeleted(fileId, true);
        }
    }

    private boolean isOwner(File file, User user) {
        return file.getRepository().getOwner().getId().equals(user.getId());
    }
}