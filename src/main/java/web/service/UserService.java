package web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.converter.RegisterUserDtoConverter;
import web.converter.RepositoryCreationDtoConverter;
import web.dto.RegisterUserDto;
import web.dto.RepositoryCreationDto;
import web.dto.RepositoryDto;
import web.entity.Repository;
import web.entity.User;
import web.entity.UserSettings;
import web.exception.EmailAlreadyExistsException;
import web.repository.RepositoryRepo;
import web.repository.UserRepo;
import web.repository.UserSettingsRepo;

@Service
public class UserService extends BaseService {

    @Autowired
    private UserRepo repo;

    @Autowired
    private UserSettingsRepo userSettingsRepo;

    @Autowired
    private RepositoryRepo repositoryRepo;

    @Autowired
    private RegisterUserDtoConverter registerUserDtoConverter;

    @Autowired
    private RepositoryCreationDtoConverter repositoryCreationDtoConverter;

    public User registerNewUser(RegisterUserDto registerUserDto) throws EmailAlreadyExistsException {
        String email = registerUserDto.getEmail();

        if(emailAlreadyExists(email))
            throw new EmailAlreadyExistsException(email);

        User user = registerUserDtoConverter.convertToEntity(registerUserDto);
        User saved = repo.save(user);

        UserSettings settings = new UserSettings();
        settings.setLanguage("en");
        settings.setTheme("light");
        settings.setUser(saved);
        settings = userSettingsRepo.save(settings);

        saved.setUserSettings(settings);

        RepositoryCreationDto repositoryCreationDto = new RepositoryCreationDto(null, "root", null, false, false);
        Repository repository = repositoryCreationDtoConverter.convertToEntity(repositoryCreationDto);
        repository.setPath("root/");
        repository.setOwner(saved);
        repositoryRepo.save(repository);

        return saved;
    }

    private boolean emailAlreadyExists(String email) {
        return repo.findByEmail(email) != null;
    }

}
