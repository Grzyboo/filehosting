package web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.dto.UserSettingsDto;
import web.repository.UserSettingsRepo;

import javax.transaction.Transactional;

@Service
public class UserSettingsService extends BaseService {

    @Autowired
    UserSettingsRepo repo;

    public UserSettingsDto getUserSettings(long userId) {
        return convert(repo.getOneByUserId(userId), UserSettingsDto.class);
    }

    @Transactional
    public void update(long userId, UserSettingsDto userSettingsDto) {
        String lang = userSettingsDto.getLanguage();
        String theme = userSettingsDto.getTheme();
        repo.updateUserSettings(userId, lang, theme);
    }
}
