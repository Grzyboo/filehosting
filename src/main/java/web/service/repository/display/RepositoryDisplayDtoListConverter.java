package web.service.repository.display;

import web.dto.RepositoryDisplayDto;
import web.entity.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class RepositoryDisplayDtoListConverter implements Consumer<Repository> {
    private static final String PREFIX = "    ";

    private class Item {
        List<Item> children = new ArrayList<>();
        String repoName;
        Repository repoReference;
        int level;

        Item(String repoName, Repository repoReference, int level) {
            this.repoName = repoName;
            this.repoReference = repoReference;
            this.level = level;
        }

        void add(String[] paths, int index, Repository repoReference) {
            Item found = null;

            for(Item item : children) {
                if(item.repoName.equals(paths[index])) {
                    found = item;
                    break;
                }
            }

            if(found == null) {
                Item item = this;

                while(!lastIndex(paths, index)) {
                    Item created = new Item(paths[index], null, index+1);
                    item.children.add(created);
                    item = created;
                    ++index;
                }

                item.repoReference = repoReference;
            }
            else {
                if(!lastIndex(paths, index))
                    found.add(paths, ++index, repoReference);
            }
        }

        private boolean lastIndex(String[] paths, int index) {
            return paths.length-1 == index;
        }

        void print() {
            displayDtos = new ArrayList<>();
            for(Item child : children)
                print(child, "");
        }

        private void print(Item item, String prefix) {
            Repository r = item.repoReference;
            if(r != null) {
                RepositoryDisplayDto dto = RepositoryDisplayDto.builder()
                        .id(r.getId())
                        .realPath(r.getPath())
                        .isPrivate(r.getIsPrivate())
                        .isProtected(r.getIsProtected())
                        .displayPath(prefix + item.repoName)
                        .build();
                displayDtos.add(dto);
            }

            for(Item child : item.children)
                print(child, prefix + PREFIX);
        }
    }

    private Item root = new Item("/", null, 0);
    private List<RepositoryDisplayDto> displayDtos;

    @Override
    public void accept(Repository repo) {
        String[] separateRepos = repo.getPath().split("/", -1);
        root.add(separateRepos, 0, repo);
    }

    public List<RepositoryDisplayDto> getAll() {
        root.print();
        return displayDtos;
    }
}
