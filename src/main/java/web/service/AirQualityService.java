package web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import web.dto.AirQualityDto;
import web.repository.AirQualityStatusRepo;

@Component
public class AirQualityService extends BaseService {
    @Autowired
    AirQualityStatusRepo repo;

    public AirQualityDto getLastStatus() {
        return convert(repo.findLast(), AirQualityDto.class);
    }

}
