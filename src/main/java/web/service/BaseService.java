package web.service;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseService {

    @Autowired
    private MapperFacade facade;

    public <ENTITY, DTO> DTO convert(ENTITY entity, Class<DTO> dtoClass) {
        return facade.map(entity, dtoClass);
    }

    public <ENTITY, DTO> List<DTO> convert(List<ENTITY> list, Class<DTO> dtoClass) {
        return list.stream().map(entity -> convert(entity, dtoClass)).collect(Collectors.toList());
    }
}