package web.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("file.hosting")
public class FileHostingProperties {
    private String uploadDir;
}
