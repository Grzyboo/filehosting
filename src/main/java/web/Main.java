package web;

import ch.qos.logback.classic.Level;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import web.properties.FileHostingProperties;

@SpringBootApplication
@EnableConfigurationProperties({FileHostingProperties.class})
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        setLoggingLevel(Level.INFO);
    }

    public static void setLoggingLevel(ch.qos.logback.classic.Level level) {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        root.setLevel(level);
    }
}