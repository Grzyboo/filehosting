package web.scheduler;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import web.entity.AirQualityStatus;
import web.repository.AirQualityStatusRepo;

import java.io.IOException;
import java.util.Date;

@Component
@EnableScheduling
@Slf4j
public class OutsideApiScheduler {
    private static final long AIR_QUALITY_UPDATE_RATE = 5*60*1000;
    private static final String AIR_QUALITY_URL = "http://api.waqi.info/feed/here/?token=" + "9dd8927814e49c73d5091db03b7363fe678195ba";

    @Autowired
    private AirQualityStatusRepo airQualityStatusRepo;

    @Scheduled(fixedDelay = AIR_QUALITY_UPDATE_RATE)
    private void updateAirQuality() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.getForEntity(AIR_QUALITY_URL, String.class);

            if (response.getStatusCode() != HttpStatus.OK)
                return;

            try {
                log.info("Air quality update");
                parseAirQuality(response.getBody());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ResourceAccessException ex) {
            ex.printStackTrace();
        }
    }

    private void parseAirQuality(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(json);

        double aqi = root.path("data").path("aqi").asDouble();
        String statusStr = root.path("status").asText();
        String city = root.path("data").path("city").path("name").asText();

        AirQualityStatus status = new AirQualityStatus();
        status.setCity(city);
        status.setStatus(statusStr);
        status.setAqi(aqi);
        status.setTime(new Date());

        airQualityStatusRepo.save(status);
    }
}
