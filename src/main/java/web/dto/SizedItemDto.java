package web.dto;

import java.text.DecimalFormat;

public abstract class SizedItemDto {
    public String getSizeStr() {
        long size = getSize();
        long kB = size / 1024;
        long MB = kB / 1024;
        long GB = MB / 1024;

        if(GB > 0)
            return "" + getFraction(MB) + "GB";
        else if(MB > 0)
            return "" + getFraction(kB) + "MB";

        return "" + getFraction(size) + "kB";
    }

    private String getFraction(long size) {
        return new DecimalFormat("0.##").format(size*1.0/1024);
    }

    public abstract Long getSize();
}
