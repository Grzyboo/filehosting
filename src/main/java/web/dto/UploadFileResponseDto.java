package web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.text.DecimalFormat;

@Data
@AllArgsConstructor
public class UploadFileResponseDto extends SizedItemDto {
    private String fileName;
    private String fileDownloadUri;
    private String fileInfoUri;
    private String fileType;
    private Long size;
}
