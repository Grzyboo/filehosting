package web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FileProtectionDto {
    private Long repositoryId;
    private String repositoryPath;
    private boolean accessGained;
}
