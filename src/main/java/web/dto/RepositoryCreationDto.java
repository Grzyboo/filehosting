package web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RepositoryCreationDto {
    Long parentId;
    String name;
    String password;
    Boolean isProtected;
    Boolean isPrivate;
}
