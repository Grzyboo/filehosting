package web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@AllArgsConstructor
public class MultipartFileInputDto {
    private MultipartFile file;
    private long repositoryId;
}
