package web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RepositoryDto {
    Long id;
    Boolean deleted;
    String path;
    Boolean isProtected;
    Boolean isPrivate;
}
