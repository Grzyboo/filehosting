package web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AirQualityDto {
    String city;
    String status;
    Double aqi;
    String time;
}
