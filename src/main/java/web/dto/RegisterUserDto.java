package web.dto;

import lombok.Data;
import web.validator.annotation.ValidEmail;
import web.validator.annotation.ValidPassword;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ValidPassword
public class RegisterUserDto {
    @NotNull
    private String displayName;

    @NotNull
    @NotEmpty
    private String password;
    private String matchingPassword;

    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;
}
