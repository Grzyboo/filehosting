package web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import web.enums.FileType;

import java.util.Date;

@Getter
@AllArgsConstructor
public class SearchResultDto extends SizedItemDto {
    Long id;

    String name;
    String extension;
    Long size;
    Date created;
    FileType fileType;
    Long ownerId;
    String ownerName;
    String repositoryPath;

    @Setter
    String uri;

    public SearchResultDto(Long id, String name, String extension, Long size, Date created, FileType fileType, Long ownerId, String ownerName, String repositoryPath) {
        this.id = id;
        this.name = name;
        this.extension = extension;
        this.size = size;
        this.created = created;
        this.fileType = fileType;
        this.ownerId = ownerId;
        this.ownerName = ownerName;
        this.repositoryPath = repositoryPath;
        this.uri = "";
    }

    public void addAccessNeededInformation() {
        name = "*" + name;
    }
}
