package web.dto;

import lombok.*;

@Getter
@Builder
public class RepositoryDisplayDto {
    private Long id;
    private String realPath;
    private Boolean isProtected;
    private Boolean isPrivate;

    private String displayPath;
    private int level;
}
