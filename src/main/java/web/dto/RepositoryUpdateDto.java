package web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RepositoryUpdateDto {
    Long id;
    String path;
    String password;
    Boolean isProtected;
    Boolean isPrivate;
}
