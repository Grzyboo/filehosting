package web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FileInfoDto {
    private SearchResultDto file;
    private String downloadUri;
    private FileProtectionDto protection;

}
