package web.enums;

public enum FileType {
    IMAGE,
    AUDIO,
    VIDEO,
    TEXT,
    ARCHIVE,
    OTHER
}
