package web.config;


import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import web.mapper.BaseMapper;

@Configuration
public class MapperConfig {

    @Bean
    public MapperFacade configure(BaseMapper[] mappers) {
        return new ConfigurableMapper() {
            @Override
            protected void configure(MapperFactory factory) {
                for(BaseMapper mapper : mappers)
                    mapper.configure(factory);
            }
        };
    }

    // This is needed at this point for project to compile.
    // TODO: Delete when at least one mapper is configured
    @Bean
    BaseMapper baseMapper() {
        return factory -> {};
    }
}
